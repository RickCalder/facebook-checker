<?php include '../components/html-head.php'; ?>
<body class="bg-grey-lightest">
  <?php include '../components/nav.php'; ?>
  <div class="container mx-auto mt-8 block">
    <h1>Privacy Notice</h1>
    <p>This site does not store any personal information. The Facebook login is for demonstration purposes only and explicit directions on how to remove access for this app is included in the content on the site</p>
    <p>To contact the site operator email: <a href="mailto:rick@calder.io">rick@calder.io</a></p>
  </div>
</body>
</html>