<?php include 'components/html-head.php'; ?>
<body class="bg-grey-lightest">
  <?php include 'components/nav.php'; ?>
  <div class="container mx-auto mt-8 block lg:flex">
    <div class="content">
      <div id="main">
      <h1>Are You Secure?</h1>
        <p>
          This is a very simple test to help you understand your online security. Fraud and identity theft are very real threats these days. Recovering from indentity theft can take years and cost you many thousands of dollars! It can destroy your credit rating preventing you from making large purchases like homes and cars.
        </p>
        <p>
          One way that people that commit identity theft are able to do this is with the things you share on the internet. They learn as much as they can about you, then use that information to create bank accounts, obtain credit cards and make credit purchases in your name, then never pay the bills leaving you with the responsibility for paying. Worse, since you never knew these things were done you only find out once the accounts are in collections, already ruining your credit rating.
        </p>
        <p>
          To find out if you're susceptible to online fraud and identity theft we've created a simple automated test. Click the button below to run this test and see the results! <span class="red">Your family's finances and welfare depend on you being vigilant in your online activities!</span> You should take this responsibility very seriously!
        </p>
        <div class="text-center">
          <div class="fb-login-button" data-max-rows="1" data-size="large" data-button-type="continue_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true" onlogin="checkLoginState();" scope="email,public_profile, user_friends"></div>
        </div>
      </div>
      <?php include 'components/whoops.php'; ?>
      <?php include 'components/yay.php'; ?>
    </div><!--end content-->
    <?php include 'components/sidebar.php'; ?>
  </div>



  <!-- scripts -->
  <script src="//cdn.jsdelivr.net/jquery/2.1.3/jquery.js"></script>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1518231614960542', 
      // appId      : '1956188981314161',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
      
    FB.AppEvents.logPageView();   
      
  };
  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      // statusChangeCallback(response);
      console.log(response)
      if( response.status === "connected") {
        $("#main").hide("fast", function(){
          $("#whoops").slideDown();
          getUserData(response.authResponse.userID)
        });
      } else {
        $("#main").hide("fast", function(){
          $("#yay").slideDown();
        });}
    });
  }

  function getUserData(id) {
    FB.api('/me', 'GET', {fields: 'first_name,last_name,name,id,email,friends'}, function(response2) {
        $("#friends").html(response2.friends.summary.total_count)
        $("#name").text(response2.name)
        $("#email").html(response2.email)
        $("#image").attr("src","https://graph.facebook.com/" + response2.id + "/picture?type=normal")
        console.log(response2)
      });
  }

  (function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  </script>


</body>
</html>