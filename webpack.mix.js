const mix = require('laravel-mix')
const tailwindcss = require('tailwindcss')
const atImport = require('postcss-import')

mix.copy('assets/src/css/**/*', 'css')
// mix.copy('resources/assets/img/**/*', 'public/img')

mix
  // .js('resources/assets/js/app.js', 'public/js')
  .postCss('assets/src/pcss/app.pcss', 'css', [
    atImport(),
    tailwindcss('./tailwind.js'),
    require('autoprefixer'),
  ])
  .disableNotifications()