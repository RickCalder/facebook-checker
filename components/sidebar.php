
<div class="sidebar">
  <h3 class="mb-4">Resources</h3>
  <p class="text-sm">
    Below is a list of other resources you can use to find out about your online security. Each link opens in a new tab.
  </p>
  <ul class="text-xs list-reset">
    <li class="mb-3">
      <a href="https://ca.norton.com/online-fraud-identity-theft/article" target="_blank">Norton - Online fraud and Identity theft</a>
    </li>
    <li class="mb-3">
      <a href="https://ca.norton.com/16-tips-for-avoiding-online-fraud-and-identity-theft/article" target="_blank">Norton - Tips for Avoiding Online Fraud</a>
    </li>
    <li class="mb-3">
      <a href="https://www.webroot.com/ca/en/home/resources/articles/pc-security/malware-identity-theft" target="_blank">Webroot - Online Identity Theft</a>
    </li>
    <li class="mb-3">
      <a href="https://www.consumer.ftc.gov/topics/online-security" target="_blank">FTC - Online Security</a>
    </li>
    <li class="mb-3">
      <a href="https://www.ontario.ca/page/how-avoid-or-recover-identity-theft" target="_blank">Ontario Government - How to avoid or recover from identity theft</a>
    </li>
  </ul>
  <h3 class="mb-4">Share this page</h3>
  <div class="text-center"><div class="fb-share-button" data-href="https://labs.calder.io/security" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flabs.calder.io%2Fsecurity&amp;src=sdkpreparse">Share</a></div></div>
  <div class="text-center mt-4">
  <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-size="large" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
</div><!--end sidebar-->