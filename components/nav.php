<nav class="flex items-center justify-between flex-wrap bg-blue-darker p-6">
  <div class="flex items-center flex-no-shrink text-white mr-6">
    <span class="text-xl tracking-tight">calder.io</span>
  </div>
  <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
    <div class="text-sm lg:flex-grow">
      <a href="https://labs.calder.io/security" class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-grey-light mr-4 no-underline">
        Home
      </a>
      <a href="./privacy" class="block mt-4 lg:inline-block lg:mt-0 text-white hover:text-grey-light mr-4 no-underline">
        Privacy
      </a>
    </div>
    <div>
      <a href="https://calder.io" target="_blank" class="inline-block text-sm px-4 py-2 leading-none border rounded text-white border-white hover:border-transparent hover:text-blue-darker hover:bg-white mt-4 lg:mt-0">Visit calder.io</a>
    </div>
  </div>
</nav>