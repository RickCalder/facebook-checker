<div id="yay">
  <h1 class="text-green">
    Well Done!
  </h1>
  <p>You didn't just give a random website access to your Facebook User Profile, Email Address and Pictures.</p>
  <p>If you had clicked continue and we were storing your login information (we aren't this is for demonstration purposes only), we could use that info to analyze your Facebook profile later, look at your pictures to determine where you live, go to school, or work. Check throough your posts and see what you're doing, perhaps if you're on vacation or planning to be away from home soon.</p>
  <p>These are just the base permissions too, there are others like birthday, home town, location, religion and politics. You can see a full list of what an app can ask for here: <a href="https://developers.facebook.com/docs/facebook-login/permissions/" target="_blank">Facebook developer login permissions</a></p>
  <h2>Who were you in a previous life?</h2>
    <p>All those silly fun Facebook quizzes and tests use this method to log you in. They get whatever information they request when you click Continue as or Login with Facebook button.</p>
    <p>Now, most of these things exist to push advertising views on their sites they're benign for the most part, but that doesn't mean they can't be used for nefarious things.</p>
    <p>Just so you understand, the total time to create this site and set up the Facebook application is a little under 2 hours. That's how easy it is for someone to set up a legitimate looking website designed with the sole purpose of stealing your personal information</p>
    <ul>
      <li>Do you read what permissions these apps are asking for?</li>
      <li>Do you post personally identifiable information to Facebook?</li>
      <li>Is your Facebook email address your real email address, or a throwaway that you use to sign up for things?</li>
      <li>Is your birthday public?</li>
      <li>Is your location public?</li>
      <li>Is your phone number public?</li>
      <li>Are your religious or political leanings public?</li>
    </ul>
    <h2>What should you do now?</h2>
    <p>The first thing you should do is immediately go to your <a href="https://www.facebook.com/settings?tab=applications" target="_blank">Facebook app security settings</a> and review all the applications (click the Show All button) that have access to your account. I promise you there are going to be a lot more than you expect. If you don't recognize an application remove its permissions to your account. Remove access entirely to this app "Online Security Tester".</p>
    <p>Also double check your <a href="https://www.facebook.com/settings?tab=privacy" target="_blank">Facebook Privacy Settings</a> and make sure you're comfortable with all those settings. Finally check your <a href="https://www.facebook.com/settings?tab=security" target="_blank">Facebook Security Settings</a>. At the very least make sure you set up to get alerts about unrecognized logins. I suggest using 2 Factor authentication as well, it can be a bit of a pain to have to receive a text message every time you want to login to your account, but far less of a pain than losing control of your account.</p>
    <p><strong>Warning</strong> Never, and I do mean NEVER click through on a site that is asking for permission to post to your timeline. They can do this at any time without your knowledge, and post ANYTHING they want, including links to viruses, support for parties you may not agree with, things that break the Facebook TOS and get your account banned. Allowing random people you don't know the ability to post to your timeline is an incredibly bad idea!</p>
    <h2>Share this page</h2>
    <p>One more thing you can do is share this page. Let your friends see how easy it is for these sites to get their personal information.</p>
    <div class="text-center"><div class="fb-share-button" data-href="https://labs.calder.io/security" data-layout="button" data-size="large" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Flabs.calder.io%2Fsecurity&amp;src=sdkpreparse">Share</a></div></div>
    <div class="text-center mt-4">
    <a href="https://twitter.com/share?ref_src=twsrc%5Etfw" class="twitter-share-button" data-size="large" data-show-count="false">Tweet</a><script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></div>
</div>